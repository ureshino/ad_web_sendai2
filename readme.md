# vulnerable_corporate_site

## Required

- PHP 5.5
- composer


## Setup

``` shell
$ git clone [repo]
$ composer install
$ php artisan migrate
```