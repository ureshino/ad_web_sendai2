<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home/index');
    }

    public function contact(){

        if(!isset($_GET['p'])){
            $_GET['p'] = 0;
        }
        
        $list = Contact::getList();
        return view('home.contact', ['list' => $list, 'total' => Contact::getTotal()]);
    }

    public function news(){
        return view('home.news');
    }

    public function news_submit(){
        file_put_contents('news/' . $_POST['title'], $_POST['body']);
        return view('home.news');
    }

    public function news_delete(Request $request){
        unlink('news/' . urlencode($request->id));
        return view('home.news');
    }
}
